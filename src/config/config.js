// default config
module.exports = {
  workers: 1,
  port: 9090,

  // 可以公开访问的Controller
  publicController: [
    // 格式为controllerpm2
    'weixin',
    'auth'
  ],

  // 可以公开访问的Action
  publicAction: [
    // 格式为： controller+action
    'admin/login'
  ],
  onUnhandledRejection: err => think.logger.error(err), // unhandledRejection handle
  onUncaughtException: err => think.logger.error(err) // uncaughtException handle
};
