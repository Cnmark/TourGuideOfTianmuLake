// production config, it will load in production enviroment
module.exports = {
  workers: 0,
  port: 6886,

  // 可以公开访问的Controller
  publicController: [
    // 格式为controllerpm2
    'weixin',
    'auth'
  ],

  // 可以公开访问的Action
  publicAction: [
    // 格式为： controller+action
    'admin/login'
  ],
  onUnhandledRejection: err => think.logger.error(err), // unhandledRejection handle
  onUncaughtException: err => think.logger.error(err) // uncaughtException handle
};
