module.exports = class extends think.Logic {
  signAction() {
    this.allowMethods = 'get';
    this.rules = {
      data: {
        string: true,
        trim: true,
        required: true
      }
    };
  };

  postTeamAction() {
    this.allowMethods = 'post';
    this.rules = {
      open_id: {
        string: true,
        trim: true,
        required: true
      },
      guide_name: {
        string: true,
        trim: true,
        required: true
      },
      full_guide_name: {
        string: true,
        trim: true,
        required: true
      },
      phone_number: {
        string: true,
        trim: true
      },
      team_name: {
        string: true,
        trim: true,
        required: true
      },
      team_number: {
        int: true,
        trim: true,
        required: true
      },
      team_nature_one: {
        int: true,
        trim: true,
        required: true
      },
      team_nature_two: {
        int: true,
        trim: true,
        required: true
      },
      team_travel: {
        int: true,
        trim: true,
        required: true
      },
      tourist_view: {
        string: true,
        trim: true,
        required: true
      },
      tourist_demand: {
        string: true,
        trim: true
      }
    };
  }
};
