const Base = require('./base.js');
const moment = require('moment');

module.exports = class extends Base {
  async menuAction() {
    return this.display();
  }

  async buildAction() {
    const teamTravel = this.model('team_travel');
    const teamNatureOne = this.model('team_nature_one');
    const teamNatureTwo = this.model('team_nature_two');
    const teamView = this.model('team_view');

    const touristData = await teamView.where({status: 1}).select();
    const travelData = await teamTravel.where({status: 1}).select();
    const natureOneData = await teamNatureOne.where({status: 1}).select();
    const natureTwoData = await teamNatureTwo.where({status: 1}).select();

    this.assign('teamView', JSON.stringify(touristData));
    this.assign('teamTravel', JSON.stringify(travelData));
    this.assign('teamNatureOne', JSON.stringify(natureOneData));
    this.assign('teamNatureTwo', JSON.stringify(natureTwoData));

    return this.display();
  }

  async signAction() {
    var json = JSON.parse(this.get('data'));
    var openId = json.open_id;
    const beacons = json.beacons;

    const data = await this.model('build_team').where({open_id: openId, status: 1}).find();
    if (data.id) {
      if (moment(data.create_time, 'YYYY-MM-DD HH:mm:ss').format('YYYY MM DD') !== moment().format('YYYY MM DD')) {
        return this.fail(103, '无法隔天签到，请重新建团');
      }
      const buildId = data.id;
      const guideName = data.guide_name;
      const openId = data.open_id;
      const remoteData = await this.model('beacon').select();
      for (let i = 0; i < beacons.length; i++) {
        for (let j = 0; j < remoteData.length; j++) {
          if (beacons[i].major == remoteData[j].major && beacons[i].minor == remoteData[j].minor) {
            const touristAttraction = remoteData[j].tourist_attraction;
            const attendRecord = this.model('attend_record');
            const record = await attendRecord.where({build_id: buildId, tourist_attraction: touristAttraction}).find();
            if (record.id) {
              return this.fail(104, '已在该签到处签到成功，无法重复签到');
            }
            const insertId = await attendRecord.add({
              open_id: openId,
              build_id: buildId,
              guide_name: guideName,
              tourist_attraction: touristAttraction,
              attend_time: moment().format('YYYY-MM-DD HH:mm:ss'),
              status: 0
            });
            if (insertId) {
              const tourist = await this.model('tourist_attraction').where({type_code: touristAttraction}).find();
              // 如果是终点结束签到
              if (tourist.status === 3) {
                await this.model('build_team').where({id: data.id}).update({status: 2});
              }
              const name = tourist.name ? tourist.name : '';
              return this.success('', '已在' + name + '签到处签到成功');
            } else {
              return this.fail(102, '系统错误，数据库无法插入');
            }
          }
        }
      }
    } else {
      return this.fail(100, '您尚未建团，无法签到');
    }
    return this.fail(101, '未识别当前签到点，请靠近签到点重试');
  }

  async postTeamAction() {
    const model = this.model('build_team');
    const array = await model.where({open_id: this.post('open_id'), status: 1}).select();
    if (array.length !== 0 && moment(array[0].create_time, 'YYYY-MM-DD HH:mm:ss').format('YYYY MM DD') ===
        moment().format('YYYY MM DD')) {
      return this.fail(101, '建团失败,今日存在尚未结束的团');
    }
    await model.where({open_id: this.post('open_id')}).update({status: 2});
    const insertId = await model.add(
      {
        open_id: this.post('open_id'),
        guide_name: this.post('guide_name'),
        team_name: this.post('team_name'),
        team_number: this.post('team_number'),
        full_guide_name: this.post('full_guide_name'),
        phone_number: this.post('phone_number'),
        tourist_demand: this.post('tourist_demand'),
        team_nature_one: this.post('team_nature_one'),
        team_nature_two: this.post('team_nature_two'),
        team_travel: this.post('team_travel'),
        tourist_view: this.post('tourist_view'),
        create_time: moment().format('YYYY-MM-DD HH:mm:ss'),
        status: 1
      });
    if (insertId > 0) {
      return this.success('', '建团成功');
    } else {
      return this.fail(100, '建团失败');
    }
  }
};
