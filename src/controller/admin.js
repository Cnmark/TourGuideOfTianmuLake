const Base = require('./base.js');

module.exports = class extends Base {
  async indexAction() {
    this.assign('username', think.userInfo.username);
    return this.display();
  }

  changepwdAction() {
    return this.display();
  }

  async changePasswordAction() {
    const oldPwd = this.post('oldPwd');
    const newPwd = this.post('newPwd');
    const userInfo = this.getLoginUserInfo();
    if (think.md5(oldPwd + 'TM_USER') !== userInfo.password) {
      return this.fail(400, '旧密码不正确');
    } else {
      userInfo.password = think.md5(newPwd + 'TM_USER');
      this.model('user').update(userInfo);
      return this.success('', '修改密码成功');
    }
  }
};
