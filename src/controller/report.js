const Base = require('./base.js');
const moment = require('moment');

module.exports = class extends Base {
  async reSignPageAction() {
    const touristAttraction = this.model('tourist_attraction');
    const data = await touristAttraction.select();
    const buildId = this.get('buildId');
    const records = await this.model('attend_record').where({build_id: buildId}).select();
    this.assign('touristAttraction', JSON.stringify(data));
    this.assign('attend_record', JSON.stringify(records));
    this.assign('build_id', buildId);
    return this.display();
  }

  async reSignAction() {
    const buildId = this.get('build_id');
    const attraction = this.get('attraction');
    const time = this.get('time');
    const data = await this.model('build_team').where({id: buildId}).find();
    const attendRecord = this.model('attend_record');
    const insertId = await attendRecord.add({
      open_id: data.open_id,
      build_id: buildId,
      guide_name: data.guide_name,
      tourist_attraction: attraction,
      attend_time: data.create_time.substring(0, 11) + time,
      status: 1
    });
    if (insertId > 0) {
      return this.success('', '补签成功');
    } else {
      return this.fail(103, '无法补签');
    }
  }

  async detailAction() {
    const touristAttraction = this.model('tourist_attraction');
    const data = await touristAttraction.select();
    this.assign('touristAttraction', JSON.stringify(data));
    const teamView = this.model('team_view');
    const teamViewData = await teamView.select();
    this.assign('teamView', JSON.stringify(teamViewData));
    return this.display();
  }

  async personAction() {
    const touristAttraction = this.model('tourist_attraction');
    const data = await touristAttraction.where({status: 1}).select();
    this.assign('touristAttraction', JSON.stringify(data));
    return this.display();
  }

  async scenicAction() {
    const touristAttraction = this.model('tourist_attraction');
    const data = await touristAttraction.where({status: 1}).select();
    this.assign('touristAttraction', JSON.stringify(data));
    return this.display();
  }

  async departmentAction() {
    const touristAttraction = this.model('tourist_attraction');
    const data = await touristAttraction.where({status: 1}).select();
    this.assign('touristAttraction', JSON.stringify(data));
    return this.display();
  }

  async detailQueryAction() {
    const result = await this.model('report').detailSimple(this.get('startDate'), this.get('endDate'));
    if (result.length === 0) {
      return this.fail(100, '未查询到数据');
    } else {
      for (let i = 0; i < result.length; i++) {
        const array = await this.model('report').detailAttend(result[i].id);
        result[i].attends = array;
      }
    }
    return this.success(result);
  }

  async personQueryAction() {
    const startDate = this.get('startDate');
    const endDate = this.get('endDate');
    const result = await this.model('report').personSimple(startDate, endDate);
    think.logger.debug(JSON.stringify(result));
    if (result.length === 0) {
      return this.fail(100, '未查询到数据');
    } else {
      for (let i = 0; i < result.length; i++) {
        const array = await this.model('report').personAttend(result[i].guide_name, startDate, endDate);
        result[i].counts = array;
      }
    }

    return this.success(result);
  }

  async travelQueryAction() {
    const startDate = this.get('startDate');
    const endDate = this.get('endDate');
    const result = await this.model('report').travelSimple(startDate, endDate);
    think.logger.debug(JSON.stringify(result));
    if (result.length === 0) {
      return this.fail(100, '未查询到数据');
    } else {
      for (let i = 0; i < result.length; i++) {
        const array = await this.model('report').travelAttend(result[i].team_name, startDate, endDate);
        result[i].counts = array;
      }
    }

    return this.success(result);
  }

  async departmentQueryAction() {
    const startDate = this.get('startDate');
    const endDate = this.get('endDate');
    const result = await this.model('report').departmentSimple(startDate, endDate);
    if (result[0].count === 0) {
      return this.fail(100, '未查询到数据');
    } else {
      for (let i = 0; i < result.length; i++) {
        const array = await this.model('report').departmentAttend(startDate, endDate);
        result[i].counts = array;
      }
    }

    return this.success(result);
  }
};
