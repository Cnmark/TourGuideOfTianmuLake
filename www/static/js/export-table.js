function HtmlExportToExcel(tableid, filename) {
  if (getExplorer() == 'ie' || getExplorer() == undefined) {
    HtmlExportToExcelForIE(tableid, filename);
  } else {
    HtmlExportToExcelForEntire(tableid, filename);
  }
}

function HtmlExportToExcelForIE(tableid, filename) {
  try {
    var curTbl = document.getElementById(tableid);
    var oXL;
    try {
      oXL = new ActiveXObject('Excel.Application'); // 创建AX对象excel
    } catch (e) {
      alert('无法启动Excel!\n\n如果您确信您的电脑中已经安装了Excel，' + '那么请调整IE的安全级别。\n\n具体操作：\n\n' + '工具 → Internet选项 → 安全 → 自定义级别 → 对没有标记为安全的ActiveX进行初始化和脚本运行 → 启用');
      return false;
    }
    var oWB = oXL.Workbooks.Add(); // 获取workbook对象
    var oSheet = oWB.ActiveSheet;// 激活当前sheet
    var sel = document.body.createTextRange();
    sel.moveToElementText(curTbl); // 把表格中的内容移到TextRange中
    try {
      sel.select(); // 全选TextRange中内容
    } catch (e1) {
      e1.description;
    }
    sel.execCommand('Copy');// 复制TextRange中内容
    oSheet.Paste();// 粘贴到活动的EXCEL中
    oXL.Visible = true; // 设置excel可见属性
    var fname = oXL.Application.GetSaveAsFilename(filename + '.xls', 'Excel Spreadsheets (*.xls), *.xls');
    oWB.SaveAs(fname);
    oWB.Close();
    oXL.Quit();
  } catch (e) {
    alert(e.description);
  }
}

// 非IE浏览器导出Excel
var HtmlExportToExcelForEntire = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,',
    template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
    base64 = function(s) {
      return window.btoa(unescape(encodeURIComponent(s)));
    },
    format = function(s, c) {
      return s.replace(/{(\w+)}/g, function(m, p) {
        return c[p];
      });
    };
  return function(table, name) {
    if (!table.nodeType) {
      table = document.getElementById(table);
    }
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
    document.getElementById('dlink').href = uri + base64(format(template, ctx));
    document.getElementById('dlink').download = name + '.xls';
    document.getElementById('dlink').click();
  };
})();

function isIE(userAgent) { // ie?
  var isIE = userAgent.indexOf('compatible') > -1 && userAgent.indexOf('MSIE') > -1; // 判断是否IE<11浏览器
  var isEdge = userAgent.indexOf('Edge') > -1 && !isIE; // 判断是否IE的Edge浏览器
  var isIE11 = userAgent.indexOf('Trident') > -1 && userAgent.indexOf('rv:11.0') > -1;
  return isEdge || isIE11 || isIE;
}

function getExplorer() {
  var explorer = window.navigator.userAgent;
  // ie
  if (isIE(explorer)) {
    return 'ie';
  }
  // firefox
  else if (explorer.indexOf('Firefox') >= 0) {
    return 'Firefox';
  }
  // Chrome
  else if (explorer.indexOf('Chrome') >= 0) {
    return 'Chrome';
  }
  // Opera
  else if (explorer.indexOf('Opera') >= 0) {
    return 'Opera';
  }
  // Safari
  else if (explorer.indexOf('Safari') >= 0) {
    return 'Safari';
  }
}
