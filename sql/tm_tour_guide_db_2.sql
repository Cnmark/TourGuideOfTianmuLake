/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3306
 Source Schema         : tm_tour_guide_db

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 05/01/2018 15:10:53
*/

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tm_attend_record
-- ----------------------------
DROP TABLE IF EXISTS `tm_attend_record`;
CREATE TABLE `tm_attend_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `open_id` varchar(255) NOT NULL,
  `build_id` int(10) unsigned NOT NULL,
  `tourist_attraction` int(10) unsigned NOT NULL,
  `attend_time` datetime NOT NULL,
  `guide_name` varchar(255) NOT NULL DEFAULT '',
  `status` int(1) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tourist_attraction` (`tourist_attraction`),
  KEY `build_id` (`build_id`),
  CONSTRAINT `tm_attend_record_ibfk_2` FOREIGN KEY (`tourist_attraction`) REFERENCES `tm_tourist_attraction` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tm_attend_record_ibfk_3` FOREIGN KEY (`build_id`) REFERENCES `tm_build_team` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_attend_record
-- ----------------------------
BEGIN;
INSERT INTO `tm_attend_record` VALUES (1, 'o5sqrjrYA2b-1Sie6v2AhcGgTnLg', 1, 3, '2018-01-01 09:05:42', '任文洁', 0);
INSERT INTO `tm_attend_record` VALUES (2, 'o5sqrjoq4Tf6I1DReqjS_CS7bEHA', 4, 1, '2018-01-01 09:25:11', '席美玲', 0);
INSERT INTO `tm_attend_record` VALUES (3, 'o5sqrjgMhU838pri1qjvgvxZ7-QY', 6, 1, '2018-01-01 09:33:49', '杨阳', 0);
INSERT INTO `tm_attend_record` VALUES (4, 'o5sqrjt3XTDPibrlKw5T8USms9zg', 5, 1, '2018-01-01 09:45:32', '任燕', 0);
INSERT INTO `tm_attend_record` VALUES (5, 'o5sqrjoq4Tf6I1DReqjS_CS7bEHA', 4, 3, '2018-01-01 09:51:08', '席美玲', 0);
INSERT INTO `tm_attend_record` VALUES (6, 'o5sqrjrYA2b-1Sie6v2AhcGgTnLg', 1, 7, '2018-01-01 10:03:12', '任文洁', 0);
INSERT INTO `tm_attend_record` VALUES (7, 'o5sqrjrYA2b-1Sie6v2AhcGgTnLg', 1, 8, '2018-01-01 10:06:14', '任文洁', 0);
INSERT INTO `tm_attend_record` VALUES (8, 'o5sqrjg2ZKMSVAZDKwn18ekA6KSY', 7, 1, '2018-01-01 10:14:47', '郑畅', 0);
INSERT INTO `tm_attend_record` VALUES (9, 'o5sqrju981HI4rQ3PR8T2Ww1dzgg', 8, 1, '2018-01-01 10:18:35', '1', 0);
INSERT INTO `tm_attend_record` VALUES (10, 'o5sqrjrYA2b-1Sie6v2AhcGgTnLg', 1, 6, '2018-01-01 10:19:30', '任文洁', 0);
INSERT INTO `tm_attend_record` VALUES (11, 'o5sqrjgMhU838pri1qjvgvxZ7-QY', 6, 8, '2018-01-01 10:21:20', '杨阳', 0);
INSERT INTO `tm_attend_record` VALUES (12, 'o5sqrjqDXvPnxHhHPC7vi01Ucf5Y', 2, 4, '2018-01-01 10:21:52', '熊银慧', 0);
INSERT INTO `tm_attend_record` VALUES (13, 'o5sqrjt3XTDPibrlKw5T8USms9zg', 5, 8, '2018-01-01 10:36:31', '任燕', 0);
INSERT INTO `tm_attend_record` VALUES (14, 'o5sqrjk7md_AoeU8PwaC4qGhkTd4', 11, 4, '2018-01-01 10:57:57', '李文武', 0);
INSERT INTO `tm_attend_record` VALUES (15, 'o5sqrjgMhU838pri1qjvgvxZ7-QY', 6, 9, '2018-01-01 11:01:05', '杨阳', 0);
INSERT INTO `tm_attend_record` VALUES (16, 'o5sqrjt3XTDPibrlKw5T8USms9zg', 5, 6, '2018-01-01 11:01:32', '任燕', 0);
INSERT INTO `tm_attend_record` VALUES (17, 'o5sqrjk7md_AoeU8PwaC4qGhkTd4', 11, 5, '2018-01-01 11:02:46', '李文武', 0);
INSERT INTO `tm_attend_record` VALUES (18, 'o5sqrjoq4Tf6I1DReqjS_CS7bEHA', 4, 4, '2018-01-01 11:05:19', '席美玲', 0);
INSERT INTO `tm_attend_record` VALUES (19, 'o5sqrjhZ98CNHydu-CmYysIoEvIU', 10, 7, '2018-01-01 11:13:26', '周瑜敏', 0);
INSERT INTO `tm_attend_record` VALUES (20, 'o5sqrjhZ98CNHydu-CmYysIoEvIU', 10, 8, '2018-01-01 11:19:55', '周瑜敏', 0);
INSERT INTO `tm_attend_record` VALUES (21, 'o5sqrjhZ98CNHydu-CmYysIoEvIU', 10, 6, '2018-01-01 11:31:34', '周瑜敏', 0);
INSERT INTO `tm_attend_record` VALUES (22, 'o5sqrjrYA2b-1Sie6v2AhcGgTnLg', 1, 2, '2018-01-01 11:35:50', '任文洁', 0);
INSERT INTO `tm_attend_record` VALUES (23, 'o5sqrjt3XTDPibrlKw5T8USms9zg', 5, 9, '2018-01-01 11:36:15', '任燕', 0);
INSERT INTO `tm_attend_record` VALUES (24, 'o5sqrjk7md_AoeU8PwaC4qGhkTd4', 11, 7, '2018-01-01 11:38:21', '李文武', 0);
INSERT INTO `tm_attend_record` VALUES (25, 'o5sqrjk7md_AoeU8PwaC4qGhkTd4', 11, 8, '2018-01-01 11:42:34', '李文武', 0);
INSERT INTO `tm_attend_record` VALUES (26, 'o5sqrjk7md_AoeU8PwaC4qGhkTd4', 11, 6, '2018-01-01 11:47:59', '李文武', 0);
INSERT INTO `tm_attend_record` VALUES (27, 'o5sqrjqDXvPnxHhHPC7vi01Ucf5Y', 2, 3, '2018-01-01 12:04:57', '熊银慧', 0);
INSERT INTO `tm_attend_record` VALUES (28, 'o5sqrjhZ98CNHydu-CmYysIoEvIU', 10, 9, '2018-01-01 12:13:13', '周瑜敏', 0);
INSERT INTO `tm_attend_record` VALUES (29, 'o5sqrjqDXvPnxHhHPC7vi01Ucf5Y', 2, 2, '2018-01-01 12:13:28', '熊银慧', 0);
INSERT INTO `tm_attend_record` VALUES (30, 'o5sqrjhZ98CNHydu-CmYysIoEvIU', 10, 2, '2018-01-01 12:39:07', '周瑜敏', 0);
INSERT INTO `tm_attend_record` VALUES (31, 'o5sqrjoq4Tf6I1DReqjS_CS7bEHA', 4, 9, '2018-01-01 13:16:16', '席美玲', 0);
INSERT INTO `tm_attend_record` VALUES (32, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 12, 1, '2018-01-01 13:23:41', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (33, 'o5sqrjpv_WfeOaW6tG8qUY77Ole4', 13, 1, '2018-01-01 13:29:20', '陈旭姣', 0);
INSERT INTO `tm_attend_record` VALUES (34, 'o5sqrjpv_WfeOaW6tG8qUY77Ole4', 13, 3, '2018-01-01 13:48:17', '陈旭姣', 0);
INSERT INTO `tm_attend_record` VALUES (35, 'o5sqrjoq4Tf6I1DReqjS_CS7bEHA', 4, 2, '2018-01-01 13:57:58', '席美玲', 0);
INSERT INTO `tm_attend_record` VALUES (36, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 12, 7, '2018-01-01 14:10:20', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (37, 'o5sqrjg2ZKMSVAZDKwn18ekA6KSY', 7, 2, '2018-01-01 14:16:52', '郑畅', 0);
INSERT INTO `tm_attend_record` VALUES (38, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 12, 8, '2018-01-01 14:17:34', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (39, 'o5sqrjt3XTDPibrlKw5T8USms9zg', 5, 2, '2018-01-01 14:19:13', '任燕', 0);
INSERT INTO `tm_attend_record` VALUES (40, 'o5sqrjk7md_AoeU8PwaC4qGhkTd4', 11, 2, '2018-01-01 14:20:59', '李文武', 0);
INSERT INTO `tm_attend_record` VALUES (41, 'o5sqrjgMhU838pri1qjvgvxZ7-QY', 6, 2, '2018-01-01 14:21:19', '杨阳', 0);
INSERT INTO `tm_attend_record` VALUES (42, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 12, 6, '2018-01-01 14:29:01', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (43, 'o5sqrjpv_WfeOaW6tG8qUY77Ole4', 13, 7, '2018-01-01 14:37:52', '陈旭姣', 0);
INSERT INTO `tm_attend_record` VALUES (44, 'o5sqrjpv_WfeOaW6tG8qUY77Ole4', 13, 8, '2018-01-01 14:43:28', '陈旭姣', 0);
INSERT INTO `tm_attend_record` VALUES (45, 'o5sqrjpv_WfeOaW6tG8qUY77Ole4', 13, 6, '2018-01-01 14:52:35', '陈旭姣', 0);
INSERT INTO `tm_attend_record` VALUES (46, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 12, 9, '2018-01-01 14:55:02', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (47, 'o5sqrju4LqXmwQHZcI-AYgKawuSo', 14, 4, '2018-01-01 15:01:38', '时幸', 0);
INSERT INTO `tm_attend_record` VALUES (48, 'o5sqrjilgVUhL1YiLYBRX8Om5Lco', 15, 9, '2018-01-01 15:13:34', '黄蕾', 0);
INSERT INTO `tm_attend_record` VALUES (49, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 12, 3, '2018-01-01 15:39:03', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (50, 'o5sqrjpv_WfeOaW6tG8qUY77Ole4', 13, 9, '2018-01-01 15:41:31', '陈旭姣', 0);
INSERT INTO `tm_attend_record` VALUES (51, 'o5sqrju4LqXmwQHZcI-AYgKawuSo', 14, 2, '2018-01-01 15:51:00', '时幸', 0);
INSERT INTO `tm_attend_record` VALUES (52, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 12, 2, '2018-01-01 15:51:54', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (53, 'o5sqrjs2x2Uhg34fN4PeDMFCP3aw', 16, 2, '2018-01-01 15:55:43', '一', 0);
INSERT INTO `tm_attend_record` VALUES (54, 'o5sqrjpv_WfeOaW6tG8qUY77Ole4', 13, 2, '2018-01-01 16:03:58', '陈旭姣', 0);
INSERT INTO `tm_attend_record` VALUES (55, 'o5sqrjlPaP5Jl4Jwrc9Ok1MXI6QI', 18, 1, '2018-01-02 08:32:57', '陈月', 0);
INSERT INTO `tm_attend_record` VALUES (56, 'o5sqrjlPaP5Jl4Jwrc9Ok1MXI6QI', 18, 3, '2018-01-02 08:47:47', '陈月', 0);
INSERT INTO `tm_attend_record` VALUES (57, 'o5sqrjlPaP5Jl4Jwrc9Ok1MXI6QI', 18, 7, '2018-01-02 11:08:07', '陈月', 0);
INSERT INTO `tm_attend_record` VALUES (58, 'o5sqrjlPaP5Jl4Jwrc9Ok1MXI6QI', 18, 8, '2018-01-02 11:12:37', '陈月', 0);
INSERT INTO `tm_attend_record` VALUES (59, 'o5sqrjlPaP5Jl4Jwrc9Ok1MXI6QI', 18, 6, '2018-01-02 11:30:59', '陈月', 0);
INSERT INTO `tm_attend_record` VALUES (60, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 20, 4, '2018-01-02 11:33:05', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (61, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 20, 5, '2018-01-02 11:39:36', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (62, 'o5sqrjlPaP5Jl4Jwrc9Ok1MXI6QI', 18, 9, '2018-01-02 11:53:28', '陈月', 0);
INSERT INTO `tm_attend_record` VALUES (63, 'o5sqrjlPaP5Jl4Jwrc9Ok1MXI6QI', 18, 2, '2018-01-02 12:17:56', '陈月', 0);
INSERT INTO `tm_attend_record` VALUES (64, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 20, 8, '2018-01-02 12:25:52', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (65, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 20, 6, '2018-01-02 12:41:18', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (66, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 20, 9, '2018-01-02 13:28:21', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (67, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', 20, 2, '2018-01-02 13:32:20', '管丽娟', 0);
INSERT INTO `tm_attend_record` VALUES (68, 'o5sqrjvlz2uCRme2dcAHAebac1ek', 21, 9, '2018-01-02 13:45:16', '刘惟敏', 0);
INSERT INTO `tm_attend_record` VALUES (69, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', 22, 4, '2018-01-03 11:36:26', '邢永昕', 0);
INSERT INTO `tm_attend_record` VALUES (70, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', 22, 8, '2018-01-03 12:46:10', '邢永昕', 0);
INSERT INTO `tm_attend_record` VALUES (71, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', 22, 2, '2018-01-03 14:12:04', '邢永昕', 0);
INSERT INTO `tm_attend_record` VALUES (72, 'o5sqrjrYA2b-1Sie6v2AhcGgTnLg', 1, 1, '2018-01-03 08:00:00', '任文洁', 0);
INSERT INTO `tm_attend_record` VALUES (73, 'o5sqrjrYA2b-1Sie6v2AhcGgTnLg', 1, 4, '2018-01-03 09:30:00', '任文洁', 0);
INSERT INTO `tm_attend_record` VALUES (74, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', 22, 1, '2018-01-04 09:02:03', '邢永昕', 0);
INSERT INTO `tm_attend_record` VALUES (75, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', 22, 3, '2018-01-04 10:00:00', '邢永昕', 0);
INSERT INTO `tm_attend_record` VALUES (76, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', 22, 5, '2018-01-04 11:00:00', '邢永昕', 0);
INSERT INTO `tm_attend_record` VALUES (77, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', 22, 6, '2018-01-04 11:30:00', '邢永昕', 0);
INSERT INTO `tm_attend_record` VALUES (78, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', 22, 7, '2018-01-04 12:00:00', '邢永昕', 0);
INSERT INTO `tm_attend_record` VALUES (79, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', 22, 9, '2018-01-04 13:00:00', '邢永昕', 0);
INSERT INTO `tm_attend_record` VALUES (80, 'o5sqrjs2x2Uhg34fN4PeDMFCP3aw', 16, 1, '2018-01-05 08:03:00', '一', 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_beacon
-- ----------------------------
DROP TABLE IF EXISTS `tm_beacon`;
CREATE TABLE `tm_beacon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tourist_attraction` int(10) unsigned NOT NULL,
  `major` int(10) NOT NULL,
  `minor` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tourist_attraction` (`tourist_attraction`),
  CONSTRAINT `tm_beacon_ibfk_1` FOREIGN KEY (`tourist_attraction`) REFERENCES `tm_tourist_attraction` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_beacon
-- ----------------------------
BEGIN;
INSERT INTO `tm_beacon` VALUES (1, 1, 10176, 25167);
INSERT INTO `tm_beacon` VALUES (2, 2, 10176, 25174);
INSERT INTO `tm_beacon` VALUES (3, 3, 10176, 25169);
INSERT INTO `tm_beacon` VALUES (4, 4, 10176, 25168);
INSERT INTO `tm_beacon` VALUES (5, 5, 10176, 25170);
INSERT INTO `tm_beacon` VALUES (6, 6, 10176, 25172);
INSERT INTO `tm_beacon` VALUES (7, 7, 10176, 25171);
INSERT INTO `tm_beacon` VALUES (8, 8, 10176, 25173);
INSERT INTO `tm_beacon` VALUES (9, 9, 10176, 25175);
COMMIT;

-- ----------------------------
-- Table structure for tm_build_team
-- ----------------------------
DROP TABLE IF EXISTS `tm_build_team`;
CREATE TABLE `tm_build_team` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guide_name` varchar(255) NOT NULL,
  `phone_number` char(11) NOT NULL,
  `full_guide_name` varchar(255) NOT NULL,
  `team_name` varchar(255) NOT NULL,
  `team_number` int(10) NOT NULL,
  `team_travel` int(10) unsigned NOT NULL,
  `team_nature_one` int(10) unsigned NOT NULL,
  `tourist_view` varchar(255) NOT NULL,
  `tourist_demand` varchar(255) DEFAULT NULL,
  `team_nature_two` int(10) unsigned NOT NULL,
  `open_id` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `team_travel` (`team_travel`),
  KEY `team_nature_one` (`team_nature_one`) USING BTREE,
  KEY `team_nature_two` (`team_nature_two`),
  CONSTRAINT `tm_build_team_ibfk_1` FOREIGN KEY (`team_travel`) REFERENCES `tm_team_travel` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tm_build_team_ibfk_2` FOREIGN KEY (`team_nature_one`) REFERENCES `tm_team_nature_one` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tm_build_team_ibfk_3` FOREIGN KEY (`team_nature_two`) REFERENCES `tm_team_nature_two` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tm_build_team
-- ----------------------------
BEGIN;
INSERT INTO `tm_build_team` VALUES (1, '任文洁', '13901496798', '张', '散客', 4, 1, 1, '1', '', 1, 'o5sqrjrYA2b-1Sie6v2AhcGgTnLg', '2018-01-01 09:05:10', 2);
INSERT INTO `tm_build_team` VALUES (2, '熊银慧', '13625116866', '赵', '溧阳散客', 3, 1, 1, '1', '', 1, 'o5sqrjqDXvPnxHhHPC7vi01Ucf5Y', '2018-01-01 09:13:17', 2);
INSERT INTO `tm_build_team` VALUES (3, '111', '15152336896', '。。。', '111', 32, 4, 3, '1,2,3,4', '', 1, 'o5sqrjlccYdYLpcxCroZW-1EOBlc', '2018-01-01 09:13:44', 1);
INSERT INTO `tm_build_team` VALUES (4, '席美玲', '13861924939', '吴', '南通天海', 15, 1, 2, '1', '', 1, 'o5sqrjoq4Tf6I1DReqjS_CS7bEHA', '2018-01-01 09:24:21', 2);
INSERT INTO `tm_build_team` VALUES (5, '任燕', '13961108213', '任燕', '消费大队签单', 10, 1, 3, '1', '包船不上龙兴岛', 1, 'o5sqrjt3XTDPibrlKw5T8USms9zg', '2018-01-01 09:27:48', 2);
INSERT INTO `tm_build_team` VALUES (6, '杨阳', '13061696972', '胡', '溧阳散客', 5, 1, 1, '1', '', 1, 'o5sqrjgMhU838pri1qjvgvxZ7-QY', '2018-01-01 09:31:28', 2);
INSERT INTO `tm_build_team` VALUES (7, '郑畅', '13813534898', '一', '测试', 20, 1, 1, '1', '没', 1, 'o5sqrjg2ZKMSVAZDKwn18ekA6KSY', '2018-01-01 10:14:21', 2);
INSERT INTO `tm_build_team` VALUES (8, '1', '13901498554', '1', '1', 1, 1, 1, '1', '', 1, 'o5sqrju981HI4rQ3PR8T2Ww1dzgg', '2018-01-01 10:18:25', 1);
INSERT INTO `tm_build_team` VALUES (9, '陈恒', '15050530918', '张', '散客', 14, 1, 1, '1', '15050530918', 1, 'o5sqrjlF9Om2n3fKTk-_EKNEoT0Q', '2018-01-01 10:19:53', 1);
INSERT INTO `tm_build_team` VALUES (10, '周瑜敏', '13764206595', '陈小姐', '上海散客', 8, 1, 1, '1', '', 1, 'o5sqrjhZ98CNHydu-CmYysIoEvIU', '2018-01-01 10:25:19', 2);
INSERT INTO `tm_build_team` VALUES (11, '李文武', '15861119160', '马', '连云港散客', 2, 1, 1, '1', '快艇   12点出门吃饭', 1, 'o5sqrjk7md_AoeU8PwaC4qGhkTd4', '2018-01-01 10:55:06', 2);
INSERT INTO `tm_build_team` VALUES (12, '管丽娟', '13901501566', '顾', '散客', 16, 1, 1, '1', '', 1, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', '2018-01-01 13:17:12', 2);
INSERT INTO `tm_build_team` VALUES (13, '陈旭姣', '18678858615', '郑先生', 'vip定制', 5, 1, 1, '1', '客人需16:30之前出园', 1, 'o5sqrjpv_WfeOaW6tG8qUY77Ole4', '2018-01-01 13:28:42', 2);
INSERT INTO `tm_build_team` VALUES (14, '时幸', '18706186329', '李', '无锡散客', 6, 1, 1, '1', '3小时出园', 1, 'o5sqrju4LqXmwQHZcI-AYgKawuSo', '2018-01-01 13:57:31', 2);
INSERT INTO `tm_build_team` VALUES (15, '黄蕾', '13775255112', '宋导', '苏州途客', 21, 4, 2, '1,2,3', '', 1, 'o5sqrjilgVUhL1YiLYBRX8Om5Lco', '2018-01-01 15:13:26', 1);
INSERT INTO `tm_build_team` VALUES (16, '一', '11111111111', '一', '一', 1, 1, 1, '1', '', 1, 'o5sqrjs2x2Uhg34fN4PeDMFCP3aw', '2018-01-01 15:55:02', 2);
INSERT INTO `tm_build_team` VALUES (17, '陈乐', '18912311585', '朱志良', '苏州吴都国旅', 16, 3, 2, '1,3', '', 1, 'o5sqrjktTKZWI1bKnwZd_SD76CiE', '2018-01-02 07:54:57', 1);
INSERT INTO `tm_build_team` VALUES (18, '陈月', '18701932938', '徐导', '蒲公英', 15, 3, 2, '1,2', '', 1, 'o5sqrjlPaP5Jl4Jwrc9Ok1MXI6QI', '2018-01-02 08:20:43', 2);
INSERT INTO `tm_build_team` VALUES (19, '陶梦姣', '18665878757', '顾导', '南通国旅', 14, 4, 2, '1,2,3', '', 1, 'o5sqrjoPb7FKri35y4tDv-yJx1_0', '2018-01-02 08:53:35', 1);
INSERT INTO `tm_build_team` VALUES (20, '管丽娟', '15961242836', '管', '营销中心签单', 6, 1, 3, '1', '', 1, 'o5sqrjth69V4qMLpuxtsg5L4p6zU', '2018-01-02 10:00:14', 2);
INSERT INTO `tm_build_team` VALUES (21, '刘惟敏', '18351508085', '韩', '无锡国旅', 59, 1, 2, '1', '', 1, 'o5sqrjvlz2uCRme2dcAHAebac1ek', '2018-01-02 10:04:34', 1);
INSERT INTO `tm_build_team` VALUES (22, '邢永昕', '13906117585', '李先生', '常州交通国际', 4, 1, 2, '1', '夕照湾', 1, 'o5sqrjoEBSTUP4pUuvH4m98Sko78', '2018-01-03 10:38:23', 2);
INSERT INTO `tm_build_team` VALUES (23, '高婧', '15216679799', '张娇', '杭州蒲公英', 16, 1, 2, '1', '自然山水', 1, 'o5sqrjgZHtoAw8KyxRuZsrabjss0', '2018-01-03 12:22:13', 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_team_nature_one
-- ----------------------------
DROP TABLE IF EXISTS `tm_team_nature_one`;
CREATE TABLE `tm_team_nature_one` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`type_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_team_nature_one
-- ----------------------------
BEGIN;
INSERT INTO `tm_team_nature_one` VALUES ('散客', 1, 1);
INSERT INTO `tm_team_nature_one` VALUES ('团队', 2, 1);
INSERT INTO `tm_team_nature_one` VALUES ('签单', 3, 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_team_nature_two
-- ----------------------------
DROP TABLE IF EXISTS `tm_team_nature_two`;
CREATE TABLE `tm_team_nature_two` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`type_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_team_nature_two
-- ----------------------------
BEGIN;
INSERT INTO `tm_team_nature_two` VALUES ('普通团', 1, 1);
INSERT INTO `tm_team_nature_two` VALUES ('老年团', 2, 1);
INSERT INTO `tm_team_nature_two` VALUES ('学生团', 3, 1);
INSERT INTO `tm_team_nature_two` VALUES ('管家团', 4, 1);
INSERT INTO `tm_team_nature_two` VALUES ('华东团', 5, 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_team_travel
-- ----------------------------
DROP TABLE IF EXISTS `tm_team_travel`;
CREATE TABLE `tm_team_travel` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_team_travel
-- ----------------------------
BEGIN;
INSERT INTO `tm_team_travel` VALUES ('一日一点', 1, 1);
INSERT INTO `tm_team_travel` VALUES ('一日两点', 2, 1);
INSERT INTO `tm_team_travel` VALUES ('两日两点', 3, 1);
INSERT INTO `tm_team_travel` VALUES ('两日三点', 4, 1);
INSERT INTO `tm_team_travel` VALUES ('其他', 5, 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_team_view
-- ----------------------------
DROP TABLE IF EXISTS `tm_team_view`;
CREATE TABLE `tm_team_view` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_team_view
-- ----------------------------
BEGIN;
INSERT INTO `tm_team_view` VALUES ('天', 1, 1);
INSERT INTO `tm_team_view` VALUES ('竹', 2, 1);
INSERT INTO `tm_team_view` VALUES ('御', 3, 1);
INSERT INTO `tm_team_view` VALUES ('水', 4, 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_tourist_attraction
-- ----------------------------
DROP TABLE IF EXISTS `tm_tourist_attraction`;
CREATE TABLE `tm_tourist_attraction` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  `person_rate` int(3) NOT NULL DEFAULT '0',
  `team_rate` int(3) NOT NULL DEFAULT '0',
  `area` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`type_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_tourist_attraction
-- ----------------------------
BEGIN;
INSERT INTO `tm_tourist_attraction` VALUES ('大门', 1, 2, 0, 0, '');
INSERT INTO `tm_tourist_attraction` VALUES ('出口', 2, 3, 0, 0, '');
INSERT INTO `tm_tourist_attraction` VALUES ('状元阁', 3, 1, 60, 30, '湖里山');
INSERT INTO `tm_tourist_attraction` VALUES ('第二休息平台', 4, 1, 60, 50, '龙兴岛');
INSERT INTO `tm_tourist_attraction` VALUES ('天目揽胜', 5, 1, 60, 18, '龙兴岛');
INSERT INTO `tm_tourist_attraction` VALUES ('天下白茶馆', 6, 1, 60, 70, '中国茶岛');
INSERT INTO `tm_tourist_attraction` VALUES ('高空飞降', 7, 1, 60, 50, '中国茶岛');
INSERT INTO `tm_tourist_attraction` VALUES ('中国茶文化苑', 8, 1, 60, 50, '中国茶岛');
INSERT INTO `tm_tourist_attraction` VALUES ('水世界/氦气球', 9, 1, 60, 85, '天目湖');
COMMIT;

-- ----------------------------
-- Table structure for tm_user
-- ----------------------------
DROP TABLE IF EXISTS `tm_user`;
CREATE TABLE `tm_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `last_login_time` int(11) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_user
-- ----------------------------
BEGIN;
INSERT INTO `tm_user` VALUES (1, 'admin', '4ecf1f9fefc2c8d88fb8fd1ec27a8aa7', 1, 1515135201, '::ffff:127.0.0.1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
