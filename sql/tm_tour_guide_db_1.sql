/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3306
 Source Schema         : tm_tour_guide_db

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 27/12/2017 14:55:05
*/

SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tm_attend_record
-- ----------------------------
DROP TABLE IF EXISTS `tm_attend_record`;
CREATE TABLE `tm_attend_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `open_id` varchar(255) NOT NULL,
  `build_id` int(10) unsigned NOT NULL,
  `tourist_attraction` int(10) unsigned NOT NULL,
  `attend_time` datetime NOT NULL,
  `guide_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `tourist_attraction` (`tourist_attraction`),
  KEY `build_id` (`build_id`),
  CONSTRAINT `tm_attend_record_ibfk_2` FOREIGN KEY (`tourist_attraction`) REFERENCES `tm_tourist_attraction` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tm_attend_record_ibfk_3` FOREIGN KEY (`build_id`) REFERENCES `tm_build_team` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tm_beacon
-- ----------------------------
DROP TABLE IF EXISTS `tm_beacon`;
CREATE TABLE `tm_beacon` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tourist_attraction` int(10) unsigned NOT NULL,
  `major` int(10) NOT NULL,
  `minor` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tourist_attraction` (`tourist_attraction`),
  CONSTRAINT `tm_beacon_ibfk_1` FOREIGN KEY (`tourist_attraction`) REFERENCES `tm_tourist_attraction` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tm_build_team
-- ----------------------------
DROP TABLE IF EXISTS `tm_build_team`;
CREATE TABLE `tm_build_team` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `guide_name` varchar(255) NOT NULL,
  `phone_number` char(11) NOT NULL,
  `full_guide_name` varchar(255) NOT NULL,
  `team_name` varchar(255) NOT NULL,
  `team_number` int(10) NOT NULL,
  `team_travel` int(10) unsigned NOT NULL,
  `team_nature_one` int(10) unsigned NOT NULL,
  `tourist_view` varchar(255) NOT NULL,
  `tourist_demand` varchar(255) DEFAULT NULL,
  `team_nature_two` int(10) unsigned NOT NULL,
  `open_id` varchar(255) NOT NULL,
  `create_time` datetime NOT NULL,
  `status` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `team_travel` (`team_travel`),
  KEY `team_nature_one` (`team_nature_one`) USING BTREE,
  KEY `team_nature_two` (`team_nature_two`),
  CONSTRAINT `tm_build_team_ibfk_1` FOREIGN KEY (`team_travel`) REFERENCES `tm_team_travel` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tm_build_team_ibfk_2` FOREIGN KEY (`team_nature_one`) REFERENCES `tm_team_nature_one` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `tm_build_team_ibfk_3` FOREIGN KEY (`team_nature_two`) REFERENCES `tm_team_nature_two` (`type_code`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Table structure for tm_team_nature_one
-- ----------------------------
DROP TABLE IF EXISTS `tm_team_nature_one`;
CREATE TABLE `tm_team_nature_one` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`type_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_team_nature_one
-- ----------------------------
BEGIN;
INSERT INTO `tm_team_nature_one` VALUES ('散客', 1, 1);
INSERT INTO `tm_team_nature_one` VALUES ('团队', 2, 1);
INSERT INTO `tm_team_nature_one` VALUES ('签单', 3, 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_team_nature_two
-- ----------------------------
DROP TABLE IF EXISTS `tm_team_nature_two`;
CREATE TABLE `tm_team_nature_two` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`type_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_team_nature_two
-- ----------------------------
BEGIN;
INSERT INTO `tm_team_nature_two` VALUES ('普通团', 1, 1);
INSERT INTO `tm_team_nature_two` VALUES ('老年团', 2, 1);
INSERT INTO `tm_team_nature_two` VALUES ('学生团', 3, 1);
INSERT INTO `tm_team_nature_two` VALUES ('管家团', 4, 1);
INSERT INTO `tm_team_nature_two` VALUES ('华东团', 5, 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_team_travel
-- ----------------------------
DROP TABLE IF EXISTS `tm_team_travel`;
CREATE TABLE `tm_team_travel` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_team_travel
-- ----------------------------
BEGIN;
INSERT INTO `tm_team_travel` VALUES ('一日一点', 1, 1);
INSERT INTO `tm_team_travel` VALUES ('一日两点', 2, 1);
INSERT INTO `tm_team_travel` VALUES ('两日两点', 3, 1);
INSERT INTO `tm_team_travel` VALUES ('两日三点', 4, 1);
INSERT INTO `tm_team_travel` VALUES ('其他', 5, 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_team_view
-- ----------------------------
DROP TABLE IF EXISTS `tm_team_view`;
CREATE TABLE `tm_team_view` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_team_view
-- ----------------------------
BEGIN;
INSERT INTO `tm_team_view` VALUES ('天', 1, 1);
INSERT INTO `tm_team_view` VALUES ('竹', 2, 1);
INSERT INTO `tm_team_view` VALUES ('御', 3, 1);
INSERT INTO `tm_team_view` VALUES ('水', 4, 1);
COMMIT;

-- ----------------------------
-- Table structure for tm_tourist_attraction
-- ----------------------------
DROP TABLE IF EXISTS `tm_tourist_attraction`;
CREATE TABLE `tm_tourist_attraction` (
  `name` varchar(255) NOT NULL,
  `type_code` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL,
  `person_rate` int(3) NOT NULL DEFAULT '0',
  `team_rate` int(3) NOT NULL DEFAULT '0',
  `area` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`type_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_tourist_attraction
-- ----------------------------
BEGIN;
INSERT INTO `tm_tourist_attraction` VALUES ('大门', 1, 2, 0, 0, '');
INSERT INTO `tm_tourist_attraction` VALUES ('出口', 2, 3, 0, 0, '');
INSERT INTO `tm_tourist_attraction` VALUES ('状元阁', 3, 1, 60, 60, '湖里山');
INSERT INTO `tm_tourist_attraction` VALUES ('第二休息平台', 4, 1, 60, 60, '龙兴岛');
INSERT INTO `tm_tourist_attraction` VALUES ('天目揽胜', 5, 1, 60, 60, '龙兴岛');
INSERT INTO `tm_tourist_attraction` VALUES ('天下白茶馆', 6, 1, 60, 60, '中国茶岛');
INSERT INTO `tm_tourist_attraction` VALUES ('高空飞降', 7, 1, 60, 60, '中国茶岛');
INSERT INTO `tm_tourist_attraction` VALUES ('中国茶文化苑', 8, 1, 60, 60, '中国茶岛');
INSERT INTO `tm_tourist_attraction` VALUES ('水世界/氦气球', 9, 1, 60, 60, '天目湖');
COMMIT;

-- ----------------------------
-- Table structure for tm_user
-- ----------------------------
DROP TABLE IF EXISTS `tm_user`;
CREATE TABLE `tm_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `last_login_time` int(11) DEFAULT NULL,
  `last_login_ip` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tm_user
-- ----------------------------
BEGIN;
INSERT INTO `tm_user` VALUES (1, 'admin', '4ecf1f9fefc2c8d88fb8fd1ec27a8aa7', 1, 1514344116, '::ffff:127.0.0.1');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
