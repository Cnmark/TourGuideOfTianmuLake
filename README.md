
Application created by [ThinkJS](http://www.thinkjs.org)

## Install dependencies

```
npm install
```

## Start server

```
npm start
```

## Deploy with pm2

Use pm2 to deploy app on production enviroment.

```
npm run compile
pm2 startOrReload pm2.json
```

##项目说明
1.数据库配置更改
```
src/config/adpter.js
    handle: mysql
```
2.端口配置
```
src/config/config.production.js
    port
```
3.签到逻辑说明
```
* 未建团无法签到
* 已在终点签到后无法签到 
* 隔天无法签到
* 同一签到点重复签到，以第一次签到成功的时间为准
* 同一个导游同时进行的团只能有一个
```
4.建团说明
```
* 当日游玩，当日建团。不可以提前建团。
* 需认真填写建团信息
```
5.景区签到点配置说明
``` 
* 入口点的status 为2
* 出口点的status 为3
* 其他景点为1
* person_rate 为个人考核的百分比 80% 填写 80即可
* team_rate 为团队考核的百分比 80% 填写 80即可
```
6.统计报表说明
``` 
* 所统计的团队信息，只能查询到已经结束的团，正在进行中的团不在统计范围内。
* 一个团队结束的标注有2个。
    1> 本次团已在结束签到点，签到成功
    2> 再次发起建团
```
7.开机自启动配置（Windows）
```
* 修改 autostart.bat 中的目录 
* 修改 pm2.json 中的 cwd 目录
```
